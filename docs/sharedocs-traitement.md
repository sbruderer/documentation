# ShareDocs - Outils de traitement

## Fonctionnement du dossier hnTools_WatchFolder

Sous l’arborescence du compte utilisateur, on trouvera un répertoire
`hnTools_watchFolder` qui contient des outils de conversion. Ce
répertoire fonctionne sur le principe de _Watch Folder_ : une fois qu’un
fichier est déposé, un processus scanne à fréquence régulière les fichiers
et crée une demande de traitement à un outil spécifique. À la fin du
traitement, un email est envoyé automatiquement à la personne qui a
déposé le fichier pour lui indiquer que le document produit est prêt. On
peut donc le récupérer. **Il est demandé de bien le supprimer du _Watch
Folder_ après utilisation.**

Le nom du répertoire et de ces sous-répertoires définit le traitement
qui est effectué dessus. La structure est de la forme
`hnTools_watchFolder / [Tool] / [Engine] / [Preset1] / [Preset2]`.

L’organisation des répertoires de traitement est la suivante :

```
- Audio
    |- ffmpeg
       |- vers choix du format audio (MP3, MP4_aac-lc, WAV)
- OCR
    |- AbbyyCloud
       |- choix du format de sortie et de la langue
    |- AbbyyServer
       |- choix du format de sortie et de la langue
    |- Abbyy FineReader station
       |- envoie du fichier vers un opérateur pour correction.
    |- Tesseract
       |- choix du format de sortie et de la langue
- PDF
    | - ghostscript
       |- choix de la compression (PDF to PDF)
    | - xpdf
       |- toText
- Video
    | - ffmpeg
       |- vers choix du format vidéo (MP4, WebM)
```

Par exemple pour lancer un OCR via Abbyy Server vers le format Word pour un document en Allemand, il faut placer le ou les fichiers dans `hnTools_watchFolder / OCR / abbyyServer / toWord / German` le résultat se trouvera dans le même répertoire avec le postfixe `_hnOCR.docx`

## Conversion/transcodage audio et Vidéo

*NOTE : ce chapitre traite des dossiers audio et vidéo.
*Le transcodage audio/vidéo utilise [FFmpeg](https://ffmpeg.org/) comme
engine par défaut. Un programme comme [HandBrake] (https://handbrake.fr/)
utilise FFmpeg comme outil de conversion et
permet d’avoir accès dans une GUI à toutes ses options.
Il est a noter qu’en vidéo, on parle de transcodage plutôt que de
conversion, car le changement de format est un changement significatif
dans la façon de coder un média et implique souvent une perte
d’information par rapport au média source, car on change sa façon de le
représenter, voire on simplifie sa représentation vers le spectre visuel
et auditif courant.
La nomenclature du nom des presets est : `[conteneur] - [codec] - [options spécifiques]`.

Les presets pour l’audio sont :

-   **toMP3** : transcodage en MP3 sans options
-   **toWAV** : transcodage en WAV (codage PCM / Pulse Code Modulation) sans option
-   **toMP4_aac-lc** : conteneur MP4 avec codec audio AAC (Low Complexity)

Pour la vidéo les presets généraux sont :

-   **toMP4_h264** : conteneur MP4 avec codec vidéo h264, codec audio AAC
-   **toMP4_h265** : conteneur MP4 avec codec vidéo h265, codec audio AAC
-   **toMP4_h264_720p** : conteneur MP4 avec codec vidéo h264, codec audio AAC, 720 lignes max de hauteur
-   **toMP4_h264_1080p** : conteneur MP4 avec codec vidéo h264, codec audio AAC, 1080 lignes max de hauteur

Pour la vidéo les presets suivants ont été faits pour la publication
d’un flux sur un site web avec une limitation du débit.

-   **toMP4_h264_1200kbps** : conteneur MP4 avec codec vidéo h264, codec audio AAC, hauteur max 552 , limitation du débit à 1200 kbps
-   **toMP4_h264_2400kbps** : conteneur MP4 avec codec vidéo h264, codec audio AAC, hauteur max 694 , limitation du débit à 2400 kbps
-   **toWebM_vp8_1200kbps** : conteneur WebM avec codec vidéo VP8, codec audio Vorbis, hauteur max 552 , limitation du débit à 1200 kbps
-   **toWebM_vp8_2400kbps** : conteneur WebM avec codec vidéo VP9, codec audio Vorbis, hauteur max 694 , limitation du débit à 2400 kbps
-   **toWebM_vp9_1200kbps** : conteneur WebM avec codec vidéo VP8, codec audio Vorbis, hauteur max 552 , limitation du débit à 1200 kbps
-   **toWebM_vp9_2400kbps** : conteneur WebM avec codec vidéo VP9, codec audio Vorbis, hauteur max 694 , limitation du débit à 2400 kbps

Pour les traitements Audio/Video, comme certains formats peuvent
produire plusieurs fichiers (comme le HLS / HTTP Live Streaming), la
source est à mettre dans le répertoire `IN` et le résultat sera placé dans
le répertoire `OUT`.

Pour l’ajout d’autres presets vous pouvez en faire la demande à
l’adresse suivante : [assistance@huma-num.fr](mailto:assistance@huma-num.fr).

## Reconnaissance de caractères (OCR)

Le résultat sera mis dans le même répertoire avec l’ajout `_hnOCR` dans le nom du fichier.

### Conditions d’utilisations des logiciels d’OCR

Ces outils sont à utiliser dans un cadre professionnel.

Les outils d’Abbyy sont soumis au paiement d’une redevance annuelle au
nombre de pages. Il est donc limité par défaut à 900 pages par
utilisateur et par an. Si un document est envoyé vers un format Word
puis ensuite vers un format texte, il sera compté deux fois. Les
demandes d’augmentation de quota sont à faire à l’adresse
[assistance@huma-num.fr](mailto:assistance@huma-num.fr). Le contrat avec Abbyy est valable jusqu’à juin
2022, au delà de cette date nous ne pouvons garantir sa reconduction.
L’ensemble des outils ne peuvent pas traiter de document crypté ou
faisant l’objet d’une protection ou d’une restriction d’accès.

L’OCR Tesseract est open source, il n’y a pas de quota dans le nombre
de fichiers.

Les tâches de traitement sont supervisées par un administrateur de la
TGIR Huma-Num qui vérifie l’état des quotas, les erreurs de fichiers,
ou les processus incohérents. Cette personne est susceptible de voir les
documents, notamment pour l’administration et la gestion du serveur
Abbyy FineReader. Concernant l’usage spécifiquement d’Abbyy Cloud, les
documents sont supprimés de leur serveur au bout de 24h, le niveau de
sécurité de leur service est décrit [sur cette page](https://www.ocrsdk.com/security/).
Pour une stricte confidentialité des documents, il est rappelé qu’aucun
document ne doit être déporté vers un service externe de quelque nature
que ce soit, mais doit être fait localement à son poste de travail dans
des conteneurs cryptés. Un conteneur crypté certifié par l’ANSSI et de
niveau EU restricted est accessible [sur cette page](https://www.zedencrypt.com/).

### Tesseract

Tesseract ne pouvant lire des fichiers PDF, une conversion PDF vers TIFF
est automatiquement faite. Tesseract ne peut pas produire de fichier
word ou excel.

### AbbyyCloud

Le montant des licences Abbyy FineReader correspond à un nombre de pages
océrisées par année. Le quota fixé est de 900 pages par utilisateur, il
se ré-initialise tous les ans au renouvellement du contrat en juin.
Quand on utilise Abbyy Cloud le fichier est envoyé sur le serveur
d’Abbyy situé en Europe dans le Cloud de Microsoft Azure. **Pour des
documents confidentiels il faut utiliser Abbyy Fine Reader serveur
(AbbyyServer).**
AbbyyCloud est fait pour traiter rapidement des petits documents. Les
fichiers ne doivent pas excéder 30Mo. Au-delà il faut utiliser Abbyy
Fine Reader serveur. Le moteur d’OCR est régulièrement mis à jour par
Abbyy. L’option oldLanguage est activée par défaut.

Déposer le fichier dans le dossier voulu (choisir le format de sortie
puis la langue du document à traiter), le résultat se récupère au même
endroit.

### AbbyyServer

Le montant des licences Abbyy FineReader correspond à un nombre de pages
océrisées par année. Le quota fixé est de 900 pages par utilisateur, il
se ré-initialise tous les ans au renouvellement du contrat en juin.
Quand on utilise Abbyy Fine Reader server le fichier est envoyé sur un
serveur interne à la TGIR qui doit pouvoir traiter des documents
jusqu’à 2 Go. Le moteur d’OCR est mis à jour une fois par an.

L’option oldLanguage dit Fraktur (ancien allemand, français, anglais,
italien espagnol) faisant l’objet d’une facturation spécifique
d’Abbyy, *elle n’est pas active par défaut sur les comptes*. Il
faut en faire la demande à l’adresse mail [assistance@huma-num.fr](mailto:assistance@huma-num.fr)
On trouvera en plus du fichier reconnu par l’OCR un fichier ayant le
même nom, mais avec la double extension `.result.xml` qui est le rapport
statistique de l’OCR

### AbbyyStation

Cet outil n’est pas activé par défaut sur les comptes, il faut en faire
la demande à l’adresse mail [assistance@huma-num.fr](mailto:assistance@huma-num.fr).

Il s’agit du même outil qu’Abbyy FineReader Server sauf qu’à la fin
du traitement le fichier est envoyé vers un opérateur pour relecture et
apprentissage. Cela implique de configurer un ordinateur sous Microsoft
Windows avec Abbyy FineReader Station pour recevoir le document et faire
de l’apprentissage d’OCR dessus.

*La TGIR n’a pas d’opérateur affecté aux tâches d’apprentissage/correction.*

## Traitement PDF

### ghostscript : compression.

L’outil pdfToPdf_xxx permet de convertir un PDF pour l’alléger
suivant trois preset qui sont :

-   **Screen** : compression la plus forte avec des images à 72 dpi
-   **Ebook** : compression la plus forte avec des images à 150 dpi
-   **Printer** : compression faible avec des images à 300 dpi
-   **Prepress** : compression faible avec des images à 300 dpi mais conservation de l’espace colorimétrique.

On trouvera les variables affectées à chaque preset [sur cette page](https://www.ghostscript.com/doc/9.23/VectorDevices.htm#PDFWRITE) dans la section `Distiller Parameters`.

### xpdf : conversion txt.

Le dossier `pdfToTexte` permet la conversion des fichiers PDF en `.txt`. Il
est fait pour les PDF qui sont déjà en mode texte, et pour lesquels il
n’est pas utile de faire de l’OCR dessus.
Déposer le fichier dans le dossier `toTexte`, le résultat sera mis
dans ce même répertoire avec l’extension `.txt`.

## Questions et Contact

Pour tout problème ou question liée à l’utilisation de Sharedocs et des
outils mentionnés, veuillez envoyer un mail à l’adresse suivante : 
[assistance@huma-num.fr](mailto:assistance@huma-num.fr).
