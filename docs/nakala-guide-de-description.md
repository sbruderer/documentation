---
lang: fr
---

# Guide pour décrire des données dans NAKALA

La qualité et la richesse de la description des données sont des critères centraux des principes [FAIR](https://doranum.fr/enjeux-benefices/principes-fair/). Cela constitue un moyen d'atteindre les objectifs visés (faire en sorte que les données soient 
faciles à trouver, accessibles, interopérables et réutilisables).
La qualité se met en oeuvre, par exemple :
    
- en utilisant des référentiels standardisés,

- en respectant les mêmes normes intellectuelles de descriptions pour un ensemble de données,

- en choisissant des champs de métadonnées les plus adaptés à l'information donnée,

La richesse se met en oeuvre en complétant le plus grand nombre possible de champs afin d'optimiser la compréhension des données.

Dans NAKALA la description est basée sur un ensemble minimal de cinq informations qui peuvent être enrichies de manière étendue et cumulative.

!!! Note
    La description des collections dans Nakala suit les mêmes principes et utilise le même modèle que les données. La principale différence est que les métadonnées obligatoires sont le Statut de la collection (privé ou public) et le Titre.

## Sommaire

- [Statut du guide de description des données dans NAKALA](#statut-du-guide-de-description-des-donnees-dans-nakala)
- [Les métadonnées dans NAKALA](#les-metadonnees-dans-nakala)
- [Principes de description des données dans NAKALA](#principes-de-description-des-donnees-dans-nakala)
- [Métadonnées obligatoires et fortement recommandées](#metadonnees-obligatoires-et-fortement-recommandees)
	- [Type de dépôt (obligatoire)](#type-de-depot-obligatoire)
	- [Titre (obligatoire)](#titre-obligatoire)
	- [Auteurs (obligatoire)](#auteurs-obligatoire)
	- [Date de création (obligatoire)](#date-de-creation-obligatoire)
	- [Licence (obligatoire)](#licence-obligatoire)
	- [Description (recommandé)](#description-recommande)
	- [Mots-clés (recommandé)](#mots-cles-recommande)
	- [Langue (recommandé)](#langue-recommande)
- [Publication des métadonnées après attribution du DOI](#publication-des-metadonnees-apres-attribution-du-doi)


## Statut du guide de description des données dans NAKALA
La version du guide actuelle propose un ensemble de conseils et bonnes pratiques pour les champs de métadonnées obligatoires et complémentaires de premier niveau. Il a vocation à être complété.

Il est possible d'effectuer des [remarques](https://gitlab.huma-num.fr/huma-num-public/documentation/-/issues/new?issue%5Bassignee_id%5D=&issue%5Bmilestone_id%5D=) pour le faire évoluer.

## Les métadonnées dans NAKALA

1. Les champs obligatoires de NAKALA :

En déposant une données dans NAKALA, vous devez compléter cinq champs de métadonnées obligatoires :

    - Titre
    - Auteur
    - Date
    - Type
    - Licence

Ces champs inspirés du Dublin-Core permettent d'avoir une description minimale de chaque donnée.

2. Les champs optionnels du vocabulaire Dublin-Core :

Aux cinq champs de la notice de description de NAKALA (inspirés du Dublin-Core), il est possible d'ajouter et/ou dupliquer tout autre champ issu du vocabulaire Dublin-Core qualifié.

Le vocabulaire Dublin-Core ("DC") est composé de:

- une base ("DC simple") de quinze rubriques de description très génériques (contributor, coverage,
creator, date, description, format, identifier, language, publisher, relation, rights, source, subject, title, type) 

- une extension ("DC qualifié") comportant 
    - des rubriques supplémentaires (audience, provenance, rightsholder...)
    - des qualificatifs de raffinement permettant de préciser les rubriques de base 
(par exemple: available, created, dateAccepted, dateCopyrighted, dateSubmitted, issued, modified, valid
sont tous des qualificatifs venant préciser la notion générique de date). 
    - des schémas d'encodage et des vocabulaires contrôlés pour exprimer les valeurs d'une rubrique
 (par exemple: DCMIType, W3CDTF...).


3. Les autres vocabulaires :

La possibilité d'implémenter dans son interface de dépôt un autre format que le Dublin-Core n'est pas disponible pour l'instant dans NAKALA.
Le déposant peut néanmoins associer dans les fichiers d'une donnée, un fichier de métadonnées propre dans le format et le vocabulaire de son choix. 
Dans ce cas d'usage, il est possible d'exploiter cette description spécifique dans une exposition web externe à NAKALA.

## Principes de description des données dans NAKALA
La description des données doit être la plus riche, précise et exacte possible.

Aux champs obligatoires, il est souhaitable de compléter la description par toute autre information connue sur la donnée :

- Privilégiez l'emploi des termes du DC qualifié quand c'est possible plutôt que ceux du DC simple;
- Lorsque le contenu d'une rubrique est exprimé dans une langue, précisez celle-ci à l'aide de l'attribut lang;
- Privilégiez, là où c'est pertinent, l'utilisation de syntaxes formelles ou de vocabulaires contrôlés
 plutôt l'emploi de textes libres;
 - Quand plusieurs informations de même nature doivent être précisés, utilisez plusieurs fois le même terme. 
 N'utilisez pas de système basé sur des caractères séparateurs.

!!! Note
    Huma-Num soutient des réseaux d'experts données disciplinaires ou métiers via son réseau de consortiums et dans les relais en région MSH. La réflexion sur la description de données devrait inclure autant que faire se peut une harmonisation disciplinaire dans le choix des vocabulaires et dans la façon de compléter les informations. Les réseaux sont présentés dans l'onglet [Consortiums et Réseaux](https://www.huma-num.fr/) 

----------

## Métadonnées obligatoires et fortement recommandées

### Type de dépôt (obligatoire)

Ce champ précise le type principal de la ressource à sélectionner dans la liste fournie par NAKALA. Il n'est pas répétable ici.

NAKALA liste les types suivants : 

- article de journal (http://purl.org/coar/resource_type/c_6501)
- article programmable (http://purl.org/coar/resource_type/c_e9a0)
- autre (http://purl.org/coar/resource_type/c_1843)
- bibliographie (http://purl.org/coar/resource_type/c_86bc)
- bulletin (http://purl.org/ontology/bibo/Series)
- carte (http://purl.org/coar/resource_type/c_12cd)
- correspondance (http://purl.org/coar/resource_type/c_0857)
- cours (http://purl.org/coar/resource_type/c_e059)
- data paper (http://purl.org/coar/resource_type/c_beb9)
- données d'enquêtes (https://w3id.org/survey-ontology#SurveyDataSet)
- exposition d'art (http://purl.org/ontology/bibo/Collection)
- fonds d'archives (http://purl.org/library/ArchiveMaterial)
- image (http://purl.org/coar/resource_type/c_c513)
- livre (http://purl.org/coar/resource_type/c_2f33)
- logiciel (http://purl.org/coar/resource_type/c_5ce6)
- manuscrit (http://purl.org/coar/resource_type/c_0040)
- page web (http://purl.org/coar/resource_type/c_7ad9)
- partition (http://purl.org/coar/resource_type/c_18cw)
- poster (http://purl.org/coar/resource_type/c_6670)
- prépublication (http://purl.org/coar/resource_type/c_816b)
- présentation (http://purl.org/coar/resource_type/c_c94f)
- périodique (http://purl.org/coar/resource_type/c_2659)
- rapport (http://purl.org/coar/resource_type/c_93fc)
- recension (http://purl.org/coar/resource_type/c_efa0)
- set de données (http://purl.org/coar/resource_type/c_ddb1)
- son (http://purl.org/coar/resource_type/c_18cc)
- texte (http://purl.org/coar/resource_type/c_18cf)
- thèse (http://purl.org/coar/resource_type/c_46ec)
- vidéo (http://purl.org/coar/resource_type/c_12ce)
- édition de sources (http://purl.org/coar/resource_type/c_ba08)

!!! liens
    Requête API pour interroger la [liste des types](https://api.nakala.fr/vocabularies/datatypes)
    (curl -X GET "https://api.nakala.fr/vocabularies/datatypes" -H  "accept: application/json")

Il est possible d'apporter des précisions sur la nature ou le genre du contenu de la ressource en utilisant le champ optionnel dcterms:type dans "ajouter une métadonnée".

### Titre (obligatoire)
Il s'agit de décrire la donnée par un titre ou un nom. Celui-ci devrait être précis et unique pour permettre au mieux de comprendre la donnée.

- Pour une photographie le sujet principal de l'image : "Tour de la Défense : vue de la construction"
- Pour un article de presse son titre : "Lancement de la construction de la Tour de la Défense"

Selon les besoins et usages des données concernées, le titre pourra comporter des mentions de date, de lieu, de personnes...  
        
Le champ titre obligatoire est répétable pour l'indiquer en différentes langues.

Pour mentionner un titre secondaire, un titre abrégé ou autre nom donné à la ressource utiliser plutôt le champ dcterms:alternative. 

!!! Note  
    Le titre de la donnée est différent du nom du ou des fichier(s) associé(s) dans le dépôt. 
    Une donnée dans NAKALA est constitué d'une notice de description accompagnée d'un ou plusieurs fichiers.
    Le nommage des fichiers de données est également à organiser et planifier. Des règles sont explicitées dans la partie [préparer les données](/nakala-preparer-ses-donnees/)

### Auteurs (obligatoire)
Dans le champ Auteur, proposé par défaut et obligatoire, nous recommandons d'indiquer le producteur de la donnée. Cependant cela n'est pas toujours adapté à la donnée déposée voire n'est pas possible (auteur inconnu).

Pour répondre aux différents besoins de description du rôle "Auteur", il est possible de :
  
- Dupliquer le champ Auteur,  
- Ajouter un champ dcterms:creator,
- Laisser le champ Auteur en anonyme et ajouter un champ optionnel dcterms:creator, par exemple dans le cas d'une donnée dont l'auteur n'est pas connu sous la forme d'un nom/prénom.

D'autres champs se rapportent à la description d'un rôle sur une donnée et peuvent répondre à un besoin de description dans "ajouter une métadonnée" :

- dcterms:publisher
- dcterms:contributor

Il est important de prendre en compte ici la façon dont la donnée pourra être citée. 

### Date de création (obligatoire) 
Mentionner ici la date de création du contenu de la ressource et non pas de la date de création de sa forme numérisée en cas de numérisation à postériori.

Si la ressource déposée ne représente qu'un avatar numérique de l'objet décrit, par exemple la numérisation d'un manuscrit ancien, indiquer la date de création de ce dernier. 

Ce champ accepte les formes suivantes du [W3CDTF](https://www.w3.org/TR/NOTE-datetime) :

- Année-Mois-jour / YYYY-MM-DD. Exemple 2021-03-02
- Année-Moi / YYYY-MM. Exemple 2021-03
- Année / YYYY. Exemple 2021.

Ce champ accepte la valeur "Inconnue".

Si les formes acceptées par W3CDTF sont trop contraignantes il est possible de laisser le champ Date en valeur inconnue et ajouter un champ optionnel dcterms:created dont le contenu n'est pas contrôlé.

### Licence (obligatoire)
Le champ Licence précise les conditions de réutilisation possible de la donnée.

Le formulaire de dépôt de NAKALA permet au déposant de sélectionner : 

- les 6 licences [Créatives Commons](https://creativecommons.org/), 
- la licence [Etalab](https://www.etalab.gouv.fr/licence-ouverte-open-licence).

Pour répondre aux autres besoins, il est possible de mentionner d'autres licences. Dans ce cas, le champ Licence de NAKALA permet de rechercher par autocomplétion une licence dans une liste d'environ 400 licences.

!!! liens
    Requête API pour interroger la [liste des licences](https://api.nakala.fr/vocabularies/licenses)
    (curl -X GET "https://api.nakala.fr/vocabularies/licenses" -H "accept: application/json")

Si la licence voulue n'est pas trouvée il est possible de demander l'ajout d'une licence en écrivant à nakala@huma-num.fr. Il faut alors donner l'intitulé de la licence et son uri.


- Référence pour aborder la question des licences :
Doranum : [
Les licences de réutilisation dans le cadre de l’Open Data:] 
(https://doranum.fr/aspects-juridiques-ethiques/les-licences-de-reutilisation-dans-le-cadre-de-lopen-data-2/)

### Description (recommandé)
Correspond à dcterms:description

Permet de décire le contenu de la ressource sous la forme d'un texte libre. Préciser la langue de la description.

Précision sur le choix des champs de description dans les champs optionnels :
- Si la description est un résumé du contenu utiliser dcterms:abstract. 
- Si la description est une table des matières utiliser dcterms:tableOfContents.

### Mots-clés (recommandé)
Correspond à dcterms:subject

Permet de décrire le ou les sujets du contenu de la ressource sous forme de mots-clefs. Pour faciliter l'usage de ce champ, il est associé aux référentiels d'[ISIDORE](https://isidore.science/vocabularies).

Les labels des concepts issus des référentiels utilisés par ISIDORE pour l'enrichissement des données (RAMEAU, Pactols, GEMET, LCSH, BNE, GéoEthno, ArchiRès, Geonames) sont recherchables par autocomplétion. Cette autocomplétion constitue une aide au choix, le déposant peut sélectionner le concept souhaité, mais peut également inscrire un concept spécifique non trouvé par autocomplétion. 
Il suffit de valider le mot (touche Entrée) pour qu'il soit pris en compte.
Il est recommandé de préciser la langue des mots-clés. 

Ce champ multivalué peut également être répété pour donner par exemple la même liste de mot-clefs dans une autre langue.

Il est également possible d'utiliser le champ optionnel dcterms:subject dans "ajouter une métadonnée" pour par exemple, indiquer un code lié à un concept issue d'un référentiel de la bibliothèque du congrès (types: dcterms:LCSH ou dcterms:LCC), ou un code issue d'une classification (types: DDC ou UDC)

### Langue (recommandé)
Correspond à dcterms:language

Si c'est pertinent, le champ Langue permet de préciser la langue de la ressource. Ce champ est facultatif et répétable. L'identification de la langue se fait en la cherchant par l'autocomplétion dans le référentiel de Nakala (liste de plus de 7000 langues vivantes ou éteintes selon les normes ISO-639-1 et ISO-639-3). 

Il est également possible d'utiliser le champ dcterms:language dans "ajouter une métadonnée" pour indiquer une "langue" qui ne ferait pas partie de ce référentiel ou pour préciser par exemple l'écriture utilisée (Code ISO-15924)

----------

## Publication des métadonnées après attribution du DOI
Chaque donnée publiée dans NAKALA se voit attribuer un Digital Object Identifier (DOI Datacite), un identifiant pérenne permettant la citation à long terme de la donnée. Cette attribution fait l'objet de l'enregistrement de métadonnées, et au titre de la pérennité des citations, ces informations ont vocation à être disponibles à long terme. 

Ainsi une donnée publiée dans NAKALA est exposée par le serveur OAI-PMH de NAKALA et publiée dans [Datacite](https://datacite.org/).

Chaque collection dans NAKALA est un SET dans l'entrepôt OAI-PMH de NAKALA : [https://api.nakala.fr/oai2](https://api.nakala.fr/oai2).

Il est donc important d'avoir des descriptions aussi claires et précises que possible des données qui sont déposées dans NAKALA.

!!! liens
    - Un outil pour formater, à partir d'un DOI, une citation dans différents modèles [DOI Citation Formatter](https://citation.crosscite.org)
    - Un outil de recherche dans les métadonnées [Datacite metadata search](https://search.datacite.org). Les métadonnées sont récupérable dans cette interface dans les formats (DataCite (XML ou JSON) et Schema.org (JSON-LD)).



