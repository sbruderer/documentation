# Documentation NAKALA

## Introduction et présentation

### À quoi sert NAKALA ?

NAKALA est un service d’Huma-Num permettant à des chercheurs, enseignants-chercheurs ou équipes de recherche de
partager, publier et valoriser tous types de données numériques documentées (fichiers textes, sons, images, vidéos,
objets 3D, etc.) dans un entrepôt sécurisé afin de les publier en accord avec les principes du _FAIR data_
(Facile à trouver, Accessible, Interopérable et Réutilisable).

![nakala_home](media/nakala/home.png){: style="width:600px; border: 1px solid grey;"}

NAKALA assure à la fois l’accessibilité aux données et aux métadonnées ainsi que leur "citabilité" dans le temps à
l’aide d’identifiants stables fournis par Huma-Num et basés sur Handle et/ou DOI.

NAKALA s’inscrit dans le Web des données permettant notamment de rendre interopérables les métadonnées, c’est-à-dire la
possibilité de pouvoir les connecter à d’autres entrepôts existants suivant ainsi la logique des données ouvertes et
liées (*Linked Open Data*).

Par ailleurs, NAKALA propose également un dispositif d’exposition des métadonnées qui permet de les référencer par des
moteurs de recherche spécialisés comme ISIDORE.

NAKALA s’inscrit dans un dispositif cohérent de services mis en place par Huma-Num pour faciliter l’accès, le
signalement, la conservation et l’archivage à long terme des données de la recherche en SHS.

La description riche, précise et harmonisée de vos données avec NAKALA permet à celles-ci d’être comprises sur le long
terme, de garantir leur traçabilité dans le temps et d’encadrer leur réutilisation.

### Dans quels cas utiliser NAKALA ?

Il est intéressant d’utiliser NAKALA dans les cas où l’on souhaite diffuser en ligne un ensemble de données et
métadonnées descriptives ayant une cohérence scientifique (corpus, collections, reportages, etc.).

Par exemple, [un fichier vidéo déposé dans NAKALA](https://nakala.fr/11280/38d484fc) peut être inséré dans des pages
Web, comme dans le cas d’un [carnet de recherche Hypothèses](https://maisondescarnets.hypotheses.org/165) ou dans un
web-documentaire.

Plusieurs solutions s’offrent à vous pour exploiter les données qui sont dans NAKALA :

- Vous utilisez le module de publication NAKALA_Press proposé par Huma-Num ;
- Vous utilisez des outils existants comme un moteur de blogs, un CMS, etc. ;
- Vous avez les compétences techniques et vous développez votre site au-dessus des APIs de NAKALA.

### Huma-Num prend en charge :

- Une copie sécurisée de vos données et de vos métadonnées sur son infrastructure ;
- L’attribution d’un identifiant stable pour permettre leur citation (Handle avant 2020 et DOI) ;
- La mise à disposition de manière interopérable de vos métadonnées basée sur les technologies du Web de données ;
- L'exposition des métadonnées des données par le protocole documentaire OAI-PMH ;
- La pérennité de l’interface web publique générée grâce au module de publication NAKALA_Press.

### En résumé, que fait NAKALA ?

- Il vous décharge de la gestion de vos données ;
- Il vous permet de les visualiser ;
- Il vous permet de les regrouper et les présenter dans des collections homogènes ;
- Il prend en charge le partage interopérable des données et des métadonnées et leur citabilité ;
- Il dissocie le stockage de données de leur présentation ;
- Il prépare le référencement des données dans ISIDORE et facilite le processus d’archivage à long terme ;
- Il permet l’éditorialisation de vos données dans un site web personnalisé de type *https://monprojet.nakala.fr* grâce
  au module de publication NAKALA_Press.

### Que ne fait pas NAKALA ?

- Il n’enrichit pas les données ;
- Il ne permet pas un stockage des données à caractères sensibles ou sous-droits.

## Utilisation de NAKALA

### Demande de création d’un compte

L'accès à NAKALA pour déposer et gérer des données nécessite de faire une demande d'accès sur le portail [HumanID](https://humanid.huma-num.fr/)

!!! info "Liens"
    - Le portail de connexion centralisée aux services d’Huma-Num HumanID : [https://humanid.huma-num.fr/](https://humanid.huma-num.fr/)
    - Comment se créer un compte sur [HumanID](https://documentation.huma-num.fr/humanid/)

Une fois connecté sur le portail HumanID, vous pouvez demander l'accès à NAKALA.

Il est également possible de parcourir, rechercher et utiliser des données publiées de NAKALA sans se connecter directement sur [https://www.nakala.fr/](https://www.nakala.fr/).

### Tester Nakala

Nakala dispose d’un environnement de test sur son interface web et ses API.

!!! info "Liens"
    - L'interface de test est disponible à l'adresse : [test.nakala.fr](https://test.nakala.fr/)
    - Les API de test sont disponibles à l'adresse : [apitest.nakala.fr](https://apitest.nakala.fr/)


!!! attention "À savoir"
    **Il n'est pas possible d'utiliser directement son compte HumanID personnel sur _test.nakala.fr_ et _apitest.nakala.fr_**. L'accès aux environnements de test se font avec **l'un des quatre comptes disponibles** dont les login, mot de passe et clé d'API sont présentés directement sur la page d'accueil de _test.nakala.fr_.

Ces environnements sont indépendants de la version de production _www.nakala.fr_.
Pour toute utilisation de NAKALA en test, nous vous recommandons d’utiliser les environnements de test. En effet, dans ces environnement les données déposées restent en interne et peuvent être réellement définitivement supprimées. Contrairement à l'environnement de production où toute donnée déposée se voit attribuer un DOI et dont il restera une trace même après suppression.

!!! attention "À savoir"
    Noter que les environnements de test sont remis à vide et les données supprimées chaque lundi matin.

### Accueil et tableau de bord

Une fois votre compte créé et votre accès au service validé, vous pourrez accéder aux différentes fonctionnalités de
NAKALA :

- Un onglet **Tableau de bord** permet de suivre les métriques de ses données de façon chiffrée : nombre de données
  déposées/publiées, consultations de ces données, téléchargements, stockage privé, fréquence des dépôts. Cette page
  vous est personnelle ;
- Un onglet **Données** permet de lister vos données ou des données que d’autres utilisateurs ont partagées avec vous ;
- Un onglet **Collections** permet de gérer vos différentes collections ainsi que celles partagées avec vous par
  d’autres utilisateurs ;
- Un onglet **Listes** permet la gestion de vos groupes d’utilisateurs afin de gérer plus facilement les droits d’accès
  et de contribution à vos dépôts ;
- Un onglet **Sites web** permet la gestion de vos différents sites NAKALA_Press.

![nakala_dashboard](media/nakala/dashboard.png){: style="width:600px; border: 1px solid grey;"}

### La distinction entre collection et données

L’ensemble des données déposées dans NAKALA peuvent être regroupées dans des collections. Une collection regroupe un ensemble de
données cohérentes. Vous pouvez ainsi avoir plusieurs collections, correspondant à différents projets de recherche et
comprenant différents contributeurs ayant des droits spécifiques au sein de ces collections. Toute collection publique constitue
un set dans l'exposition OAI-PMH de NAKALA. Ce set OAI peut, en demandant à [isidore-sources@huma-num.fr](mailto:isidore-sources@huma-num.fr),
être indexé par le moteur ISIDORE et être présenté comme une collection dans cette plateforme.

### La création d’une collection, la gestion des droits et des groupes

Lors de la création de votre collection, vous pouvez choisir :

- Si celle-ci est privée ou publique (métadonnée obligatoire) ;
- Son ou ses titres en précisant ou non leur langue (métadonnée obligatoire) ;
- Des informations complémentaires et optionnelles sur la description de cette collection, ses mots-clés, etc. ;
- Les droits afférents à cette collection et le rôle des contributeurs associés à celle-ci. À la suite de l’ajout d’un
  utilisateur ou d’une liste d’utilisateurs, un petit oeil vous permet de donner à l’utilisateur de la collection le
  rôle d’**administrateur**, d’**éditeur** ou de **lecteur**.

Pour faciliter l’ajout de membres, il est possible de créer des listes d’utilisateurs (appartenant au même projet, par
exemple) dans l’onglet **Listes** et d’ajouter ensuite directement cette liste à la collection. De même, dans les
onglets **Collections** et **Données**, la fonctionnalité **Partagées avec moi** permet d’identifier quelles données ou
collections l’utilisateur a en partage.

Une donnée peut appartenir à plusieurs collections. Il n’y a pas de hiérarchie entre les collections. Une collection
**publique** ne peut contenir que des données **publiées**. Vous n’êtes pas obligés d’avoir les droits d’administration ou
de lecture sur une donnée publiée pour pouvoir l’ajouter dans votre collection.

Les droits de gestion d’une collection sont indépendants des droits de gestion de données qu’elle contient. Cela
signifie que si vous donnez les droits de lecture à un utilisateur sur une collection qui contient des données non
publiées, vous devrez aussi donner à ce même utilisateur les droits de lecture sur les données si vous souhaitez qu’il
puisse y avoir accès.

Chaque collection publique est un set pour le serveur OAI-PMH de NAKALA.

### Déposer des données dans NAKALA

Une donnée NAKALA est constituée d'un ou plusieurs fichiers et de métadonnées descriptives.
Le dépôt d'une donnée dans NAKALA peut se faire de deux façons différentes :

- Soit par l’interface web à travers les fonctionnalités **glisser** / **déposer** / **naviguer** où l’utilisateur
  sélectionne le ou les fichiers qu’il souhaite déposer pour la donnée dans NAKALA.

![Depot](media/nakala/submit.png){: style="width:600px; border: 1px solid grey;"}

- Soit via l’[API de NAKALA](https://apitest.nakala.fr/doc) : dans ce cas, chaque fichier de donnée associé aux
  métadonnées est à déposer avant via `POST /uploads` puis les métadonnées afférentes déposées grâce à l’onglet `POST /datas`.
  L’utilisation de l’API est à privilégier pour la mise en place d’un dépôt par lot en attendant la mise à disposition
  de cette fonctionnalité directement depuis l’interface web (en cours de développement à Huma-Num).

![Depot2](media/nakala/api-upload.png){: style="width:600px; border: 1px solid grey;"}

![Depot3](media/nakala/api-data.png){: style="width:600px; border: 1px solid grey;"}

### Visibilité des fichiers et embargo

Lors du dépôt d’une donnée, il est possible de gérer la date de visibilité des fichiers de cette donnée. Par défaut, les fichiers sont
visibles dès le dépôt. On peut décider qu’ils soient rendus publiques à partir d’une certaine date, ou jamais.
Lorsqu’un fichier est sous embargo, seuls les utilisateurs ayant au minimum les droits lecteur sur la donnée
peuvent consulter les fichiers sous embargo de cette donnée. Les métadonnées restent publiques.

![Embargo](media/nakala/embargo.png){: style="width:600px; border: 1px solid grey;"}

### Distinction données déposées / données publiées

Après avoir dûment rempli les métadonnées correspondantes aux données, l’utilisateur a la possibilité de **déposer** ses
données ou de les **publier**.

- Le **dépôt** rend visible la donnée et ses fichiers uniquement aux utilisateurs ayant un droit d’accès dessus. Il
  s’agit d’un état transitoire avant publication définitive (ex: étape de relecture). Chaque utilisateur dispose d’un
  espace de stockage limité pour ces données non-publiées (1000 données ou 2 Go). Une donnée non publiée ne peut pas appartenir à une
  collection publique et ne peut pas être éditorialisée dans un site NAKALA_Press. Une donnée déposée peut être supprimée.
- La **publication** rend la donnée accessible et réutilisable par tous. Le **DOI est publié** chez Datacite. Il n’y a pas de limite de stockage spécifique
  pour les données publiées. Une donnée peut être publiée avec des fichiers sous embargo si seules les métadonnées
  doivent être accessibles librement. La publication d’une donnée est définitive. Il **n’est pas possible de supprimer** ou
  de dépublier une donnée qui a été publiée à moins de faire une demande spécifique à l’équipe d’Huma-Num.

### Modèle de données de NAKALA et format des métadonnées

L’utilisateur peut décrire ses données selon plusieurs vocabulaires (NAKALA, DublinCore et FOAF pour l'instant). Vous pouvez proposer à
l’équipe d’Huma-Num de nouveaux vocabulaires pour compléter cette liste.

Cinq métadonnées sont obligatoires pour décrire une donnée :

- **Titre** (nakala:title, multivaluée)
- **Type** (nakala:type, unique)
- **Auteur** (nakala:creator, multivaluée)
- **Date** (nakala:created, unique)
- **Licence** (nakala:license, unique)

Ces métadonnées doivent être exprimées au moment du dépôt dans le vocabulaire NAKALA, mais peuvent être converties en
DublinCore au moment du requêtage des données via l’API ou via le protocole OAI-PMH.

Il est possible d’ajouter plusieurs titres nakala:title en précisant ou non la langue de chacun. NAKALA propose une liste de plus de 7000
langues vivantes ou éteintes selon les normes ISO-639-1 et ISO-639-3.

NAKALA propose en autocomplétion la base des auteurs en fonction des dépôts effectués par les utilisateurs. Si un auteur
n’est pas disponible dans l’autocomplétion proposée depuis l’interface web, il est possible d’en ajouter un nouveau en
cliquant sur "Ajouter d’autres auteurs" en bas du résultat de l’autocomplétion. Chaque auteur peut être décrit par son
nom, son prénom et éventuellement un identifiant [ORCID](https://orcid.org/). Lorsque l’auteur ne peut pas être
renseigné, il est possible de cocher la case "Anonyme" via l’interface web ou de renseigner une valeur à *null* via l’API.
Il est possible de renseigner d’autres formes auteurs en ajoutant des métadonnées issues du vocabulaire DublinCore.

Le format de date attendu pour nakala:created est de type AAAA, AAAA-MM ou AAAA-MM-JJ. Lorsque la date est inconnue ou ne peut pas être
renseignée, il est possible de cocher la case "Inconnue" via l’interface web ou de renseigner une valeur à *null* via
l’API. Il est possible d’indiquer d’autres formats de date en ajoutant par exemple une métadonnée dans le vocabulaire
DublinCore.

Les métadonnées nakala:type et nakala:licence doivent contenir des valeurs issues des référentiels utilisés dans NAKALA. Le
référentiel des licences contient actuellement plus de 400 valeurs. Vous pouvez proposer à l’équipe d’Huma-Num de
nouvelles valeurs pour compléter cette liste si besoin.

#### Les types de données dans NAKALA
Les types des données NAKALA sont identiques à ceux utilisés dans le moteur de recherche [ISIDORE](https://documentation.huma-num.fr/isidore/). La liste est disponible dans le tableau ci-dessous
ou via l'API à l'URL <https://api.nakala.fr/vocabularies/datatypes>.


Type | URI
--- | ---
image|http://purl.org/coar/resource_type/c_c513
video|http://purl.org/coar/resource_type/c_12ce
son|http://purl.org/coar/resource_type/c_18cc
publication|http://purl.org/coar/resource_type/c_6501
poster|http://purl.org/coar/resource_type/c_6670
présentation|http://purl.org/coar/resource_type/c_c94f
cours|http://purl.org/coar/resource_type/c_e059
livre|http://purl.org/coar/resource_type/c_2f33
carte|http://purl.org/coar/resource_type/c_12cd
dataset|http://purl.org/coar/resource_type/c_ddb1
logiciel|http://purl.org/coar/resource_type/c_5ce6
autres|http://purl.org/coar/resource_type/c_1843
fonds d'archives|http://purl.org/library/ArchiveMaterial
exposition d'art|http://purl.org/ontology/bibo/Collection
bibliographie|http://purl.org/coar/resource_type/c_86bc
bulletin|http://purl.org/ontology/bibo/Series
édition de sources|http://purl.org/coar/resource_type/c_ba08
manuscrit|http://purl.org/coar/resource_type/c_0040
correspondance|http://purl.org/coar/resource_type/c_0857
rapport|http://purl.org/coar/resource_type/c_93fc
périodique|http://purl.org/coar/resource_type/c_2659
prépublication|http://purl.org/coar/resource_type/c_816b
recension|http://purl.org/coar/resource_type/c_efa0
partition|http://purl.org/coar/resource_type/c_18cw
données d'enquêtes|https://w3id.org/survey-ontology#SurveyDataSet
texte|http://purl.org/coar/resource_type/c_18cf
thèse|http://purl.org/coar/resource_type/c_46ec
page web|http://purl.org/coar/resource_type/c_7ad9
data paper|http://purl.org/coar/resource_type/c_beb9
article programmable|http://purl.org/coar/resource_type/c_e9a0


Ces URI sont par ailleurs alignées avec l'ontologie des types ISIDORE (dont les labels sont en anglais, français et espagnol) et peut être obtenu en requêtant [le 3store d'ISIDORE](https://isidore.science/sqe) avec la SPARQL [suivante](https://isidore.science/sparql?default-graph-uri=&query=SELECT+*+WHERE+%7B%0D%0A%3FTypeISIDORE+skos%3AinScheme+%3Chttp%3A%2F%2Fisidore.science%2Fontology%3E.%0D%0A%3FTypeISIDORE+skos%3AprefLabel+%3FLabelISIDORE.%0D%0A%3FTypeISIDORE+owl%3AequivalentClass+%3FTypeCOARpourNAKALA%0D%0A%0D%0A%7D&format=text%2Fhtml&timeout=0&debug=on) :
```
SELECT * WHERE {
?TypeISIDORE skos:inScheme <http://isidore.science/ontology>.
?TypeISIDORE skos:prefLabel ?LabelISIDORE.
?TypeISIDORE owl:equivalentClass ?TypeCOARpourNAKALA
}
```


#### l'indexation par mots-clés dans NAKALA

La métadonnée "Mots-clés", associée au vocabulaire dcterms:subject, est liée aux différents thesaurus utilisés dans [ISIDORE](https://isidore.science/vocabularies).
En effet, NAKALA propose ici en autocomplétion tous les labels (prefLabel et altLabel) des concepts des référentiels qu'utilisent ISIDORE
pour enrichir ses données : le vocabulaire Rameau, les thésaurus Pactols, GEMET, LCSH, BNE, GéoEthno, Archires et enfin le référentiel géographique Geonames.

![meta_subject](media/nakala/meta_subject.png){: style="width:600px; border: 1px solid grey;"}

Aujourd'hui, seul le label du concept est enregistré dans les métadonnées de la données. C'est donc au déposant d'ajouter
les labels dans plusieurs langues si nécessaire.

!!! Note
    Une évolution prévue de NAKALA est d'enregistrer au lieu du label l'objet concept lui-même et d'afficher ou exporter ensuite toutes les informations du concept (labels, liens, ...).

Lors du dépôt, la donnée peut être intégrée dans une ou plusieurs collections en respectant la contrainte qu'une collection
publique ne peut regrouper que des données publiées.

![in-collection](media/nakala/in-collections.png){: style="width:600px; border: 1px solid grey;"}

### Attribution d’un identifiant pérenne : DOI.

NAKALA fournit automatiquement un identifiant unique et stable à chaque donnée déposée, un DOI. Il n’y a donc rien à
faire de particulier pour obtenir un identifiant avec NAKALA. Dès qu’une donnée est publiée, les droits de
lecture sont remontés dans le moteur de recherche, le serveur OAI-PMH et publiés dans Datacite.

Vous pourrez accéder à votre donnée par un lien constitué de la manière
suivante : ``https://nakala.fr/*identifiant*``

Par exemple si la donnée a pour identifiant 11280/000028fb, le lien pour accéder ou citer la donnée
est : <https://nakala.fr/11280/000028fb>.

### Utiliser et accéder aux données

L’accès à NAKALA se fait via l’adresse <https://nakala.fr>.

NAKALA propose, lorsque l’utilisateur n’est pas connecté, une page d’exploration et de recherche
des données permettant de les visualiser et de les réutiliser. Par exemple : <https://nakala.fr/11280/d6dfc55a>

La page de présentation d’un objet est <https://nakala.fr/{identifiant}>, où {identifiant}
correspondant à un Handle ou un DOI.

Les URLs des collections sont accessibles sur <https://nakala.fr/collection/{identifiant}>.

NAKALA dispose de son propre entrepôt OAI-PMH dont l’adresse Web est : <https://api.nakala.fr/oai2>. Chaque collection
de NAKALA correspond à un set OAI. Ainsi, tous les utilisateurs de l’espace NAKALA peuvent utiliser l’adresse Web de
l’entrepôt OAI-PMH pour faire moissonner les métadonnées par des portails
documentaires ([ISIDORE](https://isidore.science/), [Gallica](https://gallica.bnf.fr/), etc.).

### Les rôles dans NAKALA

Les rôles d'un utilisateur sur une donnée ou une collection sont :

- Déposant (`ROLE_DEPOSITOR`) ;
- Propriétaire (`ROLE_OWNER`) ;
- Administrateur (`ROLE_ADMIN`) ;
- Éditeur (`ROLE_EDITOR`) ;
- Lecteur (`ROLE_READER`) ;
- Anonyme (GUEST).

#### Droits sur les données

Ces rôles permettent d'effectuer les actions suivantes sur une donnée :

|Actions|ROLE_OWNER|ROLE_ADMIN|ROLE_EDITOR|ROLE_READER|GUEST|
| :-----| :-----: | :------: | :------: | :------: | :------: |
| Consultation d'une donnée publiée | + | + | + | + | + |
| Consultation d'une donnée déposée | + | + | + | + | - |
| Consultation d'une ancienne version (1)| + | + | + | + | + |
| Consultation d'une donnée supprimée (2)| + | + | + | + | + |
| Modification des métadonnées d'une donnée | + | + | + | - | - |
| Modification des métadonnées d'une donnée supprimée ou ancienne version | - | - | - | - | - |
| Publication d'une donnée déposée (3) | + | + | - | - | - |
| Modification des droits d'accès à une donnée (4) | + | + | - | - | - |
| Suppression d'une donnée déposée | + | + | - | - | - |
| Suppression d'une donnée publiée (5) | - | - | - | - | - |
| Suppression d'une ancienne version | - | - | - | - | - |
| Suppression d'une donnée supprimée | - | - | - | - | - |

1. Il s'agit de la consultation d'une ancienne version d'une donnée publiée
2. Seules certaines informations sont consultables (ex: titre, date de la suppression, déposant)
3. Pour être publiée, une donnée doit avoir un certain nombre de métadonnées obligatoires (`nakala:title`, `nakala:creator`, `nakala:created`, `nakala:type`, `nakala:license`)
4. Contrairement au `ROLE_ADMIN`, le `ROLE_OWNER` ne peut pas être supprimé.
5. La suppression d'une donnée publiée ne peut se faire que sur demande aux administrateur de NAKALA.


#### Droits sur les fichiers composant une donnée

Les droits associées aux fichiers d'une donnée dépendent des critères suivants :
 
- le rôle de l'utilisateur sur la donnée (cf. plus haut)
- le status de la donnée (déposée, publiée, ancienne version, supprimée)
- la présence ou non d'une date d'embargo en cours sur le fichier

|Actions|ROLE_OWNER|ROLE_ADMIN|ROLE_EDITOR|ROLE_READER|GUEST|
| :-----| :-----: | :------: | :------: | :------: | :------: |
| Consultation d'un fichier ouvert d'une donnée publiée | + | + | + | + | + |
| Consultation d'un fichier sous embargo d'une donnée publiée | + | + | + | + | - |
| Consultation d'un fichier d'une donnée déposée (indépendant de son embargo) | + | + | + | + | - |
| Consultation d'un fichier ouvert d'une ancienne version d'une donnée | + | + | + | + | + |
| Consultation d'un fichier sous embargo d'une ancienne version d'une donnée | + | + | + | + | - |
| Consultation d'un fichier d'une donnée supprimée (indépendant de son embargo) | - | - | - | - | - |
| Modification des fichiers associés à une donnée déposée ou publiée | + | + | + | - | - |
| Modification des fichiers associés à une ancienne version ou à une donnée supprimée | - | - | - | - | - |


#### Droits sur les collections

Les droits associées à une collection dépendent des critères suivants :

- le rôle qu'a l'utilisateur sur la collection (`ROLE_OWNER`, `ROLE_ADMIN`, `ROLE_EDITOR`, `ROLE_READER`, GUEST)
- le status de la collection (privée, publique)

|Actions|ROLE_OWNER|ROLE_ADMIN|ROLE_EDITOR|ROLE_READER|GUEST|
| :-----| :-----: | :------: | :------: | :------: | :------: |
| Consultation d'une collection publique | + | + | + | + | + |
| Consultation d'une collection privée | + | + | + | + | - |
| Modification des métadonnées | + | + | + | - | - |
| Publication d'une collection privée (1) | + | + | - | - | - |
| Dépublication d'une collection publique | + | + | - | - | - |
| Modification des droits d'accès à une collection (2) | + | + | - | - | - |
| Suppression d'une collection (indépendamment de son status) | + | + | - | - | - |

1) Pour pouvoir être publiée, une collection ne doit contenir que des données déjà publiées
2) Contrairement au `ROLE_ADMIN`, le `ROLE_OWNER` ne peut pas être supprimé.

Les possibilités d'ajout d'une donnée dans une collection dépendent des critères suivants :

- le status de la donnée (déposée, publiée, ancienne version, supprimée)
- le rôle d'un utilisateur sur la collection (`ROLE_OWNER`, `ROLE_ADMIN`, `ROLE_EDITOR`, `ROLE_READER`, GUEST)
- le status de la collection (privée, publique)

|Actions|ROLE_OWNER|ROLE_ADMIN|ROLE_EDITOR|ROLE_READER|GUEST|
| :-----| :-----: | :------: | :------: | :------: | :------: |
| Ajout ou suppression d'une donnée publiée dans une collection | + | + | + | - | - |
| Ajout ou suppression d'une donnée déposée dans une collection privée | + | + | + | - | - |
| Ajout ou suppression d'une donnée déposée dans une collection publique | - | - | - | - | - |
| Ajout ou suppression d'une ancienne version d'une donnée d'une donnée supprimée dans une collection (indépendammant de son status) | - | - | - | - | - |


#### Droits sur les listes d'utilisateurs

Les droits sur les listes d'utilisateurs dépendent des critères suivants :

- le propriétaire de la liste
- les personnes appartenant à la liste

|Actions|Propriétaire|Membre|Guest|
| :-----| :-----: | :------: | :------: |
| Ajout ou suppression d'une personne dans une liste | + | - | - |
| Renommage d'une liste | + | - | - |
| Voir les membres d'une liste | + | + | - |
| Utiliser la liste lors de l'attribution de droits sur une donnée ou une collection | + | + | - |


#### Droits sur les sites NAKALA_PRESS

Les droits sur les sites NAKALA_PRESS dépendent des critères suivants :

- les droits sur la collection associée au site web (`ROLE_OWNER`, `ROLE_ADMIN`, `ROLE_EDITOR`, `ROLE_READER`, Guest)
- le status du site web (publié ou non)

|Actions|ROLE_OWNER|ROLE_ADMIN|ROLE_EDITOR|ROLE_READER|GUEST|
| :-----| :-----: | :------: | :------: | :------: | :------: |
| Consultation d'un site publié | + | + | + | + | + |
| Consultation d'un site non publié | + | + | - | - | - |
| Édition de la configuration d'un site | + | + | - | - | - |
| Envoi d'un fichier pour l'édition d'un site | + | + | - | - | - |
| Publication d'un site | + | + | - | - | - |
| Suppression d'un site | + | + | - | - | - |


### Connexion avec Zotero

Une connexion entre NAKALA et [Zotero](https://www.zotero.org/) est proposée et vous permet grâce au module web de Zotero
de référencer une donnée de NAKALA dans votre gestionnaire bibliographique avec toutes les métadonnées correspondantes.

### Suppression des données et archivage

NAKALA n’offre pas la possibilité de supprimer directement une donnée publiée. Lorsque les fichiers d’une donnée publiée
sont modifiés, une nouvelle version de la donnée est générée et toutes les anciennes versions restent accessibles. La
modification uniquement des métadonnées n’entraine pas la création d’une nouvelle version. Si l’utilisateur souhaite
voir une donnée publiée supprimée, il doit en faire la demande à <nakala@huma-num.fr>. Le DOI de la donnée sera
cependant toujours conservé, même si elle est notée comme supprimée.

Les données déposées dans NAKALA n’entrent pas automatiquement dans un processus d’archivage à long terme. Pour mettre
en place ce processus, vous devez prendre contact avec les membres d’Huma-Num via <cogrid@huma-num.fr>.

### Si Huma-Num disparaît, comment sera assurée la citabilité des données déposées dans NAKALA ?

Comme NAKALA utilise le système de nommage DOI pour attribuer un identifiant unique à chaque donnée, il sera toujours
possible de transférer la base des identifiants à toute une autre structure qui aura en charge la suite du dispositif (
par exemple la BnF, CINES, Archives Nationales, etc).
