# Bienvenue sur la documentation des services de la TGIR Huma-Num

Vous trouverez ici la présentation et la documentation des services mis en place par la TGIR Huma-Num.

Pour en savoir plus sur la TGIR Huma-Num et son actualité, rendez-vous [sur son site web (huma-num.fr)](http://huma-num.fr) et sur  [son carnet de recherche (humanum.hypotheses.org)](https://humanum.hypotheses.org/).


## Comment contacter la TGIR Huma-Num

- Pour **un besoin d'assistance** sur un service délivré par la TGIR Huma-Num : [assistance@huma-num.fr](mailto:assistance@huma-num.fr)
- Pour **l'ouverture d'un service** et faire part de votre projet : [cogrid@huma-num.fr](mailto:cogrid@huma-num.fr)
- Pour **toute autre question** sur la TGIR Huma-Num : [contact@huma-num.fr](mailto:contact@huma-num.fr)


!!! note
    Cette documentation est en édition continue. Elle fait l'objet de [mises à jour régulières](https://gitlab.huma-num.fr/huma-num-public/documentation/-/commits/master). Vous pouvez y contribuer [en signalant un manque ou une erreur](https://gitlab.huma-num.fr/huma-num-public/documentation/-/issues/new?issue%5Bassignee_id%5D=&issue%5Bmilestone_id%5D=), ou [en éditant vous-même la documentation (voir le protocole)](https://gitlab.huma-num.fr/huma-num-public/documentation/-/tree/master#contribuer)
