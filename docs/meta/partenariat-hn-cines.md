---
lang: fr
---


# Partenariat entre Huma-Num et le CINES

La TGIR Huma-Num propose à la communauté des producteurs de données numériques en Sciences Humaines et Sociales un service de préservation sur le long terme. Elle s’appuie, pour cette activité, sur les infrastructures et les compétences d’un centre labellisé, le Centre Informatique National de l’Enseignement Supérieur ([CINES](https://www.cines.fr/)).  
Les relations entre Huma-Num et le CINES sont régies par une convention d'une durée de 4 ans. 

## Comité de liaison 
Le lien entre le CINES et Huma-Num est assuré par un "comité de liaison" composé :

- Pour le CNRS
    - Un/Une représentant.e de la direction de Huma-Num;
    - Le/La responsable du pôle données de Huma-Num; 
    - Le/La responsable du pôle national de conservation des données et documents du CNRS;
    - Qui pourront être assistés selon les sujets traités par le/la DPO (Délégué à la protection des données) du CNRS ou des structures concernées.
- Pour le CINES
    - Le responsable du Département Archivage et Diffusion;
    - Le référent des projets d’archives;
    - L’archiviste en charge de la mise en œuvre des projets avec les services versants;
    - Qui pourront être assistés selon les sujets traités par le RSSI du CINES et le DPO du CINES.


## Rôle de Huma-Num 

Dans le cadre de son partenariat avec le CINES, Huma-Num assure le lien avec les communautés SHS et les accompagne dans leur projet de préservation. Huma-Num assure le financement, identifie de nouveaux formats de données à préserver, les propose au CINES et participe au processus d’intégration de ces formats sur la plateforme du CINES. 

Une étude a par exemple été menée par Huma-Num en collaboration avec les experts de la communauté TEI pour introduire ce format au CINES. Le niveau d’exigence défini pour l’intégration de ce format au CINES a permis en retour à l’ensemble des producteurs utilisant ce format d’améliorer la qualité de leur production en particulier leur structuration et leur documentation. 

## Rôle du CINES

Le CINES apporte son expertise technique sur les formats, effectue un suivi archivistique et prend la responsabilité de la conservation sur le long-terme des données qui lui sont confiées pour le compte d’Huma-Num, en anticipant les risques liés à l’obsolescence technologique par des procédures d’assurance qualité et une planification de la préservation, et en répliquant les données sur un site distant pour parer à tout type de sinistre.  
Pour en savoir plus sur le service d'archivage proposé par le CINES :  
[https://www.cines.fr/archivage/](https://www.cines.fr/archivage/)


## Projets en cours

Début 2021, 8 projets d'archivage (hors NAKALA) sont en cours :
 

![Projets archivages CINES](../media/meta/archivage_cines.png)

