---
lang: en
---

!!! Note  
    Document in progress


# The Huma-Num Grid Committee 

The missions of the Huma-Num Grid Committee are as follows: 
- Review and process all service opening requests in accordance with Huma-Num's [general policy](https://documentation.huma-num.fr/humanum-en/) for the use of Huma-Num services. The follow-up of the requests is done with a [ticket management](https://support.huma-num.fr) tool that allows structuring the exchanges with the requesters; 
- deal with the evolution of the offer for access to already open services when necessary (e.g. overflow);
- analyze all issues related to the maintenance and evolution of services;
- to refer to the scientific council in case of need for a scientific opinion on the validity of a request. 

The committee is composed of members representing the different poles of Huma-Num and at least one representative of the management. 

The committee meets every week. 
